
# Hinweise zum Skript

Auf diesem Branch befindet sich das Skript zur VL Logik- & Modelltheorie, so wie von Prof. Droste vorgetragen.
Auf dem Branch [logik_modelltheorie_neu](https://git.informatik.uni-leipzig.de/fl34gufe/skripte/tree/logik_modelltheorie_neu) befindet sich eine überarbeitete Version des Skripts, die Beweise ergänzt, Definitionen und Lemmata stärker trennt und andere (in meinen Augen) Klarstellungen vornimmt.

Beide Branches werden aktuell gehalten.

**logik_modelltheorie_neu enthält veränderte Nummerierungen.**
